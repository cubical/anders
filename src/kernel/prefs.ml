let trace           : bool ref = ref false
let preeval         : bool ref = ref true
let girard          : bool ref = ref false
let impredicativity : bool ref = ref false
let irrelevance     : bool ref = ref false